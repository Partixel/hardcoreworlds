package io.github.elitejynx.HardcoreWorlds;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author EliteJynx
 */
public class HardcoreWorlds extends JavaPlugin implements Listener {

	// Files
	public File UsersFolder;

	// User storage
	public HashMap<Player, ArrayList<World>> UserDeaths = new HashMap<Player, ArrayList<World>>();

	public ArrayList<World> Worlds = new ArrayList<World>();

	public void addCommand(String Label, String Permission) {
		PluginCommand cmd = getCommand(Label);
		cmd.setExecutor(this);
		cmd.setPermission(Permission);
		cmd.setPermissionMessage(ChatColor.DARK_RED + "You don't have "
				+ cmd.getPermission());
		cmd.setTabCompleter(this);
	}

	public void setupCommands() {
		addCommand("GoToHardcore", "HardcoreWorlds.Commands.GoToHardcore");
	}

	@Override
	public void onEnable() {

		saveDefaultConfig();

		List<String> ConfigWorlds = getConfig().getStringList("HardcoreWorlds");
		for (String W : ConfigWorlds) {
			World w = getServer().getWorld(W);
			if (w != null)
				Worlds.add(w);
		}
		new UpdateHandler(this, 68440, getFile(),
				UpdateHandler.UpdateType.NO_DOWNLOAD, true);
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(new Spectator(), this);

		setupCommands();
	}

	@Override
	public void onDisable() {
	}

	public boolean GoToHardcore(CommandSender Sender, Command Cmd,
			String label, String[] Args) {
		if (!(Sender instanceof Player)) {
			Sender.sendMessage("You must be a player to use this");
			return true;
		}
		if (Args.length == 1) {
			World world = getServer().getWorld(Args[0]);
			if (Worlds.contains(world) && world != null) {
				((Player) Sender).teleport(world.getSpawnLocation());
			} else {
				Sender.sendMessage("That is not a valid hardcore world");
			}
		} else if (Args.length == 0 && Worlds.size() == 1) {
			World world = Worlds.get(0);
			if (world != null) {
				((Player) Sender).teleport(world.getSpawnLocation());
			} else {
				Sender.sendMessage("No valid hardcore worlds!");
			}
		}
		return true;
	}

	@Override
	public boolean onCommand(CommandSender Sender, Command Cmd, String Label,
			String[] Args) {
		if (Cmd.getName().equalsIgnoreCase("GoToHardcore")) {
			return GoToHardcore(Sender, Cmd, Label, Args);
		}
		return true;
	}

	@Override
	public ArrayList<String> onTabComplete(CommandSender Sender, Command Cmd,
			String Label, String[] Args) {
		if (Cmd.getName().equalsIgnoreCase("GoToHardcore")) {
			if (Args.length == 1) {
				ArrayList<String> UseWorlds = new ArrayList<String>();
				for (World world : Worlds) {
					if (world.getName().toLowerCase()
							.startsWith(Args[Args.length - 1].toLowerCase())) {
						UseWorlds.add(world.getName());
					}
				}
				return UseWorlds;
			}
		}
		return new ArrayList<String>();
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void OnDeath(PlayerDeathEvent Event) {
		if (Worlds.contains(Event.getEntity().getWorld())) {
			ArrayList<World> Deaths = UserDeaths.get(Event.getEntity());
			if (Deaths == null)
				Deaths = new ArrayList<World>();
			if (!Deaths.contains(Event.getEntity().getWorld()))
				Deaths.add(Event.getEntity().getWorld());
			UserDeaths.put(Event.getEntity(), Deaths);
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void OnTP(PlayerTeleportEvent Event) {
		if (Worlds.contains(Event.getTo().getWorld())) {
			ArrayList<World> Deaths = UserDeaths.get(Event.getPlayer());
			if (Deaths == null)
				Deaths = new ArrayList<World>();
			if (Deaths.contains(Event.getTo().getWorld())) {
				Spectator.addSpectator(Event.getPlayer());
				Event.getPlayer().sendMessage("You are now spectating!");
			}
		}
	}
}
