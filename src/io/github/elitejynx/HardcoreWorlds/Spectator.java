package io.github.elitejynx.HardcoreWorlds;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class Spectator implements Listener {
	private static List<Player> spectators = new ArrayList<Player>();

	public static void addSpectator(Player player) {
		if (!spectators.contains(player)) {
			spectators.add(player);
			player.setAllowFlight(true);
			for (Player Plr : Bukkit.getServer().getOnlinePlayers())
				Plr.hidePlayer(player);
		}
	}

	public static void removeSpectator(Player player) {
		if (!isSpectator(player))
			return;
		spectators.remove(player);
		player.setAllowFlight(false);
		for (Player Plr : Bukkit.getServer().getOnlinePlayers())
			Plr.showPlayer(Plr);
	}

	public static boolean isSpectator(Player player) {
		return spectators.contains(player);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerJoin(PlayerJoinEvent event) {
		for (Player Plr : Bukkit.getServer().getOnlinePlayers())
			Plr.hidePlayer(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onItemPickup(PlayerPickupItemEvent event) {
		if (isSpectator(event.getPlayer()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onItemDrop(PlayerDropItemEvent event) {
		if (isSpectator(event.getPlayer()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onInteract(PlayerInteractEvent event) {
		if (isSpectator(event.getPlayer()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onInteractEntity(PlayerInteractEntityEvent event) {
		if (isSpectator(event.getPlayer()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		if (isSpectator((Player) event.getEntity()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityDamage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player)
			if (isSpectator((Player) event.getEntity()))
				event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerDamage(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player) {
			if (isSpectator((Player) event.getEntity())) {
				event.setCancelled(true);
			}
		}
		if (event.getDamager() instanceof Player) {
			if (isSpectator((Player) event.getDamager())) {
				event.setCancelled(true);
			}
		}
	}
}
